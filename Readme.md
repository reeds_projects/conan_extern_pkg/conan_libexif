[![pipeline status](https://gitlab.com/reeds_projects/conan_extern_pkg/conan_libexif/badges/master/pipeline.svg)](https://gitlab.com/reeds_projects/conan_extern_pkg/conan_libexif/-/commits/master)

## Conan package recipe for [*libexif*](https://github.com/libexif/libexif)

libexif is a library for parsing, editing, and saving EXIF data. It is
intended to replace lots of redundant implementations in command-line
utilities and programs with GUIs.

You can read more about this [here](https://libexif.github.io/api/).


## Issues

If you wish to report an issue or make a request for a package, please do so here:

[Issues Tracker](https://gitlab.com/groups/reeds_projects/conan_extern_pkg/-/issues)


## For Users

### Basic setup

$ conan install libexif/0.6.22@/ --remote=gitlab

### Project setup

If you handle multiple dependencies in your project is better to add a *conanfile.txt*

    [requires]
    libexif/0.6.22@reeds_projects+conan_extern_pkg+conan_libexif/release

    [generators]
    cmake

Complete the installation of requirements for your project running:

    $ mkdir build && cd build && conan install ..

Note: It is recommended that you run conan install from a build directory and not the root of the project directory.  This is because conan generates *conanbuildinfo* files specific to a single build configuration which by default comes from an autodetected default profile located in ~/.conan/profiles/default .  If you pass different build configuration options to conan install, it will generate different *conanbuildinfo* files.  Thus, they should not be added to the root of the project, nor committed to git.


## Build and package

The following command both runs all the steps of the conan file, and publishes the package to the local system cache.  This includes downloading dependencies from "build_requires" and "requires" , and then running the build() method.

    $ conan create . reeds_projects+conan_extern_pkg+conan_libexif/release --build missing


### Available Options
| Option        | Default | Possible Values  |
| ------------- |:----------------- |:------------:|
| shared      | False |  [True, False] |
| fPIC      | True |  [True, False] |


## Add Remote

Conan Community has its own Bintray repository, however, we are working to distribute all package in the Conan Center:

    $ conan remote add gitlab https://gitlab.com/api/v4/packages/conan


## Conan Recipe License

NOTE: The conan recipe license applies only to the files of this recipe, which can be used to build and package libexif.
It does *not* in any way apply or is related to the actual software being packaged.

[LGPL-3.0](LICENSE)

