from conans import ConanFile, tools , AutoToolsBuildEnvironment
from conans.errors import ConanInvalidConfiguration
import glob
import os
import shutil

class libgphotoConan(ConanFile):
    build_policy = "missing"
    name = "libexif"
    version = "0.6.22"
    license = "LGPL"
    description = "libexif is a library for parsing, editing, and saving EXIF data. It is intended to replace lots of redundant implementations in command-line utilities and programs with GUIs."
    homepage = "https://github.com/libexif/libexif"
    author = "Benoît ROPARS <benoit.ropars@solutionsreeds.fr>"
    topics = ("conan", "libexif")
    url = "https://gitlab.com/reeds_projects/conan_extern_pkg/conan_libexif.git"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}
    #requires = ["opencv/4.3.0@conan/stable"]
    #requires = ("gstreamer/1.16.0@bincrafters/stable")
    exports = "LICENSE"
    generators = "pkg_config"
    
    _source_subfolder = "source_subfolder"
    

    def configure(self):
        del self.settings.compiler.libcxx

    def source(self):
        zip_name = "%s-%s" % (self.name, self.version)
        tools.get("https://github.com/libexif/libexif/releases/download/%s-release/%s.tar.gz" % (zip_name.replace(".", "_"),zip_name))
        os.rename(zip_name, self._source_subfolder)

    def build(self):
        autotools = AutoToolsBuildEnvironment(self)
        
        prefix = os.path.abspath(self.package_folder)
        args = ['--prefix=%s' % prefix]
        
        if self.options.shared:
            args.extend(['--disable-static', '--enable-shared'])
        else:
            args.extend(['--enable-static', '--disable-shared'])
            
        args.extend(['--disable-dependency-tracking'])
            
        with tools.chdir(self._source_subfolder):
            if self.settings.os == "Macos":
                tools.replace_in_file("configure", r'-install_name \$rpath/\$soname', r'-install_name \$soname')

            self.run('chmod +x configure')
            
            with tools.environment_append(autotools.vars):
                self.run("autoreconf -fi")
            
            autotools.configure(args=args)
            autotools.make()
            autotools.make(args=['install'])

    def package(self):
        self.copy(pattern="COPYING*", dst="licenses", src=self._source_subfolder, ignore_case=True, keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["exif"]
        

